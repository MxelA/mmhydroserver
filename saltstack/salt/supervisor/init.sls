/etc/supervisor/conf.d/mmhydro.conf:
    file.managed:
        - user: vagrant
        - group: vagrant
        - mode: 644
        - contents:
            - "[program:laravel-worker]"
            - process_name=%(program_name)s_%(process_num)02d
            - user=vagrant
            - command=php /home/vagrant/mmhydro/artisan queue:work --tries=1 --daemon
            - numprocs=1
            - user=vagrant
            - autostart=true
            - autorestart=true
            - stdout_logfile=/home/vagrant/mmhydro/storage/logs/laravel-worker.out.log
            - stderr_logfile=/home/vagrant/mmhydro/storage/logs/laravel-worker.err.log

/etc/supervisor/conf.d/mmhydro-device-publisher-processing-data.conf:
    file.managed:
        - user: vagrant
        - group: vagrant
        - mode: 644
        - contents:
            - "[program:laravel-worker-device-publisher-processing-data]"
            - process_name=%(program_name)s_%(process_num)02d
            - user=vagrant
            - command=php /home/vagrant/mmhydro/artisan queue:work --tries=1 --queue=device_publisher_processing_data --daemon
            - numprocs=1
            - user=vagrant
            - autostart=true
            - autorestart=true
            - stdout_logfile=/home/vagrant/mmhydro/storage/logs/laravel-worker-device-publisher-processing-data.out.log
            - stderr_logfile=/home/vagrant/mmhydro/storage/logs/laravel-worker-device-publisher-processing-data.err.log

/etc/supervisor/conf.d/mmhydro-device-worker-configure-action.conf:
    file.managed:
        - user: vagrant
        - group: vagrant
        - mode: 644
        - contents:
            - "[program:laravel-worker-device-worker-configure-action]"
            - process_name=%(program_name)s_%(process_num)02d
            - user=vagrant
            - command=php /home/vagrant/mmhydro/artisan queue:work --tries=1 --queue=device_worker_configure_actions --daemon
            - numprocs=1
            - user=vagrant
            - autostart=true
            - autorestart=true
            - stdout_logfile=/home/vagrant/mmhydro/storage/logs/laravel-worker-device-worker-configure-action.out.log
            - stderr_logfile=/home/vagrant/mmhydro/storage/logs/laravel-worker-device-worker-configure-action.err.log

supervisor:
    pkg:
        - installed
    service.running:
        - watch:
            - file: /etc/supervisor/conf.d/mmhydro.conf
            - file: /etc/supervisor/conf.d/mmhydro-device-publisher-processing-data.conf
            - file: /etc/supervisor/conf.d/mmhydro-device-worker-configure-action.conf
