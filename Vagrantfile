# -*- mode: ruby -*-
# vi: set ft=ruby :

require 'json'
require 'yaml'

VAGRANTFILE_API_VERSION ||= "2"
confDir = $confDir ||= File.expand_path("vendor/laravel/homestead", File.dirname(__FILE__))

homesteadYamlPath = "Homestead.yaml"
homesteadJsonPath = "Homestead.json"
afterScriptPath = "after.sh"
aliasesPath = "aliases"

require File.expand_path(confDir + '/scripts/homestead.rb')

Vagrant.require_version '>= 1.8.4'

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
    if File.exist? aliasesPath then
        config.vm.provision "file", source: aliasesPath, destination: "~/.bash_aliases"
    end

    if File.exist? homesteadYamlPath then
        settings = YAML::load(File.read(homesteadYamlPath))
    elsif File.exist? homesteadJsonPath then
        settings = JSON.parse(File.read(homesteadJsonPath))
    end

    Homestead.configure(config, settings)

    if File.exist? afterScriptPath then
        config.vm.provision "shell", path: afterScriptPath, privileged: false
    end

    if defined? VagrantPlugins::HostsUpdater
        config.hostsupdater.aliases = settings['sites'].map { |site| site['map'] }
    end

     config.vm.network "public_network", :bridge => "192.168.100.155", :ip => "192.168.100.177"

     #config.hostmanager.enabled = true
     #config.hostmanager.manage_host = true

     # SaltStack
     config.vm.provision :salt do |salt|
         # Relative location of configuration file to use for minion
         # since we need to tell our minion to run in masterless mode
         salt.minion_config = "saltstack/etc/minion"

         # On provision, run state.highstate (which installs packages, services, etc).
         # Highstate basicly means "comapre the VMs current machine state against
         # what it should be and make changes if necessary".
         salt.run_highstate = true

         # What version of salt to install, and from where.
         # Because by default it will install the latest, its better to explicetly
         # choose when to upgrade what version of salt to use.

         # I also prefer to install from git so I can specify a version.
         salt.install_type = "git"
         salt.install_args = "v2014.1.0"

         # Run in verbose mode, so it will output all debug info to the console.
         # This is nice to have when you are testing things out. Once you know they
         # work well you can comment this line out.
         salt.verbose = true
       end

     config.vm.provision "shell", run: "always", privileged: false, inline: <<-EOF
             echo "Welcome to MM HydroGarden development environment!"
             echo "Local server address is http://mmhydro.dev"
             echo "README.md for further info."
     EOF
end
