<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/**
 * User Factory
 */
$factory->define(\App\Models\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->state(\App\Models\User::class, 'customer', function ($faker) {
    return [
        'role' => \App\Models\User::ROLE_CUSTOMER
    ];
});

//$factory->state(\App\Models\User::class, 'device', function ($faker) {
//    return [
//        'role' => \App\Models\User::ROLE_DEVICE
//    ];
//});




/**
 * Area
 */
$factory->define(\App\Models\Area::class, function (Faker\Generator $faker) {
    static $user_id;

    return [
        'name'    => 'House ' . $faker->randomDigit,
        'user_id' => $user_id ? $user_id : 2,
    ];
});

/**
 * Device type
 */

$factory->define(\App\Models\DeviceGroup::class, function (Faker\Generator $faker) {
    static $area_id;

    return [
        'name'          => 'Temp',
        'data_type'     => 'integer',
        'device_type'   => \App\Models\Device::DEVICE_TYPE_PUBLISHER,
        'area_id'       => $area_id ? $area_id : null,
    ];
});

