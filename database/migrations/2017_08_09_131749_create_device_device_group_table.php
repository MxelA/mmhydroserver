<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeviceDeviceGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device_device_group', function (Blueprint $table) {
            $table->unsignedInteger('device_id');
            $table->unsignedInteger('device_group_id');

            $table->foreign('device_id')
                ->references('id')
                ->on('devices');

            $table->foreign('device_group_id')
                ->references('id')
                ->on('device_groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('device_device_group');
    }
}
