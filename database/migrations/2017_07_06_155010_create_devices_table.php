<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devices', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('area_id')->nullable();

            $table->string('name', 50);
            $table->string('device_type', 50);
            $table->string('data_type', 50);
            $table->string('icon', 50);
            $table->string('unit', 100)->nullable();
            $table->string('token', 255)->unique();
            $table->boolean('manual_control')->nullable();
            $table->boolean('turn_on')->nullable();
            $table->json('additional_data')->nullable();
            $table->timestamps();

            $table->foreign('area_id')
                ->references('id')
                ->on('areas');

            $table->index('token');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devices');
    }
}
