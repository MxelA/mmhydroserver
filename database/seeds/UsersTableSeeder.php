<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::create([
            'name'              => 'Admin',
            'email'             => 'admin@h2m.com',
            'password'          => bcrypt('secret'),
            'remember_token'    => str_random(16),
            'role'              => \App\Models\User::ROLE_ADMIN
        ])->assign(\App\Models\User::ROLE_ADMIN);

        \App\Models\User::create([
            'name'              => 'H2M',
            'email'             => 'h2m@h2m.com',
            'password'          => bcrypt('secret'),
            'remember_token'    => str_random(16),
            'role'              => \App\Models\User::ROLE_CUSTOMER
        ])->assign(\App\Models\User::ROLE_CUSTOMER);


        \App\Models\User::create([
            'name'              => 'Hidroponika',
            'email'             => 'hidroponika@admin.com',
            'password'          => bcrypt('secret'),
            'remember_token'    => str_random(16),
            'role'              => \App\Models\User::ROLE_CUSTOMER
        ])->assign(\App\Models\User::ROLE_CUSTOMER);



//        //Make 20 customers
//        factory(App\Models\User::class, 20)->states('customer')->create()->each(function ($customer){
//            $customer->assign(\App\Models\User::ROLE_CUSTOMER);
//
//            $area = factory(\App\Models\Area::class)->create([
//                'user_id' => $customer->id
//            ]);
//
//            factory(\App\Models\DeviceGroup::class)->create([
//                'area_id' => $area->id
//            ]);
//        });


////        $device = \App\Models\User::create([
////            'name'              => 'Temp sensor',
////            'email'             => 'device@admin.com',
////            'password'          => bcrypt('secret'),
////            'remember_token'    => str_random(16),
////            'role'              => \App\Models\User::ROLE_DEVICE,
////        ])->assign(\App\Models\User::ROLE_DEVICE);
//
//        $token = \JWTAuth::fromUser($device,['exp' => \Carbon\Carbon::parse('1.1.2100')->timestamp]);
//        $device->update(['jwt_token' => $token]);

//        factory(App\Models\User::class, 20)->states('device')->create()->each(function ($device){
//            $device->assign(\App\Models\User::ROLE_DEVICE);
//
//            $token = \JWTAuth::fromUser($device,['exp' => \Carbon\Carbon::parse('1.1.2100')->timestamp]);
//            $device->update(['jwt_token' => $token]);
//        });
    }
}
