<?php

use Illuminate\Database\Seeder;

class DevicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $area = \App\Models\Area::find(1);

        $area->areaDevices()->create([
            'name'          => 'Senzor-Temparature',
            'device_type'   => \App\Models\Device::DEVICE_TYPE_PUBLISHER,
            'data_type'     => \App\Models\Device::DEVICE_DATA_TYPE_FLOAT,
            'icon'          => 'wb_sunny',
            'token'         => 'I1nDic2n20WjWdrIHJDxTSLYKejK6U',
            'unit'          => '(celsius)'
        ]);

        $area->areaDevices()->create([
            'name'          => 'Senzor-Vlaznosti',
            'device_type'   => \App\Models\Device::DEVICE_TYPE_PUBLISHER,
            'data_type'     => \App\Models\Device::DEVICE_DATA_TYPE_FLOAT,
            'icon'          => 'grain',
            'token'         => '8nJ836Zhq8pWRY7XLhQQlA7e5qZ6qJ',
            'unit'          => '(humidity)'
        ]);

        $area->areaDevices()->create([
            'name'          => 'Senzor-Svijetlosti',
            'device_type'   => \App\Models\Device::DEVICE_TYPE_PUBLISHER,
            'data_type'     => \App\Models\Device::DEVICE_DATA_TYPE_FLOAT,
            'icon'          => 'highlight',
            'token'         => 'Xf1wanMHzU19dfJplmCvDLfp6QehvU',
            'unit'          => '(lux)'
        ]);

        $area->areaDevices()->create([
            'name'          => 'Uticnica-Grijanje',
            'device_type'   => \App\Models\Device::DEVICE_TYPE_WORKER,
            'data_type'     => \App\Models\Device::DEVICE_DATA_TYPE_BOOLEAN,
            'icon'          => 'input',
            'token'         => 'cVZKjBY4dr5O8TZr6FVIxSjkTeiOaD',
            'turn_on'       => false
        ]);

        $area->areaDevices()->create([
            'name'          => 'Uticnica-Svijetlo',
            'device_type'   => \App\Models\Device::DEVICE_TYPE_WORKER,
            'data_type'     => \App\Models\Device::DEVICE_DATA_TYPE_BOOLEAN,
            'icon'          => 'input',
            'token'         => 'zcATwl1vqBf9BWFLTCqWE6NiIIV7N6',
            'turn_on'       => false
        ]);

        $area->areaDevices()->create([
            'name'          => 'Uticnica-3',
            'device_type'   => \App\Models\Device::DEVICE_TYPE_WORKER,
            'data_type'     => \App\Models\Device::DEVICE_DATA_TYPE_BOOLEAN,
            'icon'          => 'input',
            'token'         => 'disfGuuDpjfz7diUzNJxIlfK2l5aFe',
            'turn_on'       => false
        ]);

        $areaAdmin = \App\Models\Area::find(2);

        $areaAdmin->areaDevices()->create([
            'name'          => 'Uticnica-1',
            'device_type'   => \App\Models\Device::DEVICE_TYPE_WORKER,
            'data_type'     => \App\Models\Device::DEVICE_DATA_TYPE_BOOLEAN,
            'icon'          => 'input',
            'token'         => 'cVzWjBY4dr5O8TZr6fVIxSjkTeiAaD',
            'turn_on'       => false
        ]);
    }
}
