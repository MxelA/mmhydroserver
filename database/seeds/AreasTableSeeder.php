<?php

use Illuminate\Database\Seeder;

class AreasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userH2m = \App\Models\User::where('email', 'h2m@h2m.com')->first();
        $userAdmin = \App\Models\User::where('email', 'admin@h2m.com')->first();

        $userH2m->areas()->create([
            'name' => 'Plastenik Test',
            'additional_data' => [
                'time_zone' => 'Europe/Belgrade'
            ]
        ]);

        $userAdmin->areas()->create([
            'name' => 'Plastenik Test',
            'additional_data' => [
                'time_zone' => 'Europe/Belgrade'
            ]
        ]);
    }
}
