<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return \File::get(public_path() . '/h2m-app/dist/index.html');
});

Route::get('/static/css/{name}', function ($name = null) {
    $response = Response::make(\File::get(public_path() . '/h2m-app/dist/static/css/'. $name));
    $response->header('Content-Type', 'text/css');
    return $response;
});

Route::get('/static/js/{name}', function ($name = null) {
    return \File::get(public_path() . '/h2m-app/dist/static/js/'. $name);
});

Route::get('/static/image/{name}', function ($name = null) {
    return \File::get(public_path() . '/h2m-app/dist/static/image/'. $name);
});