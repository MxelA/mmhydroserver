<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



//Version 1 API
Route::group(['prefix' => 'v1'], function () {

    Route::post('/login',['uses' => 'Auth\AuthenticateController@authenticate']);

    Route::group(['middleware' => ['auth.jwt']], function () {

        //Area
        Route::group(['prefix' => 'area', 'as' => 'area.'], function (){

            Route::get('user/{user}', ['uses' => 'Customer\AreaController@areasUser']);

            Route::get('device/{area}', ['uses' => 'Customer\AreaController@areaDevices']);
            Route::get('device/publisher/{area}', ['uses' => 'Customer\AreaController@areaPublisherDevices']);
            Route::get('device/worker/{area}', ['uses' => 'Customer\AreaController@areaWorkerDevices']);
            Route::post('device/{area}', ['uses' => 'Customer\AreaController@areaDeviceStore']);

            Route::get('configure-action/{area}', ['uses' => 'Customer\AreaController@areaConfigureActionIndex']);
            Route::post('configure-action/{area}', ['uses' => 'Customer\AreaController@areaConfigureActionStore']);

            Route::get('device-group/{area}', ['uses' => 'Customer\AreaController@areaDeviceGroupIndex']);
            Route::post('device-group/{area}', ['uses' => 'Customer\AreaController@areaDeviceGroupStore']);

            Route::post('{user}', ['uses' => 'Customer\AreaController@store', 'as' => 'store']);
            Route::resource('', 'Customer\AreaController', ['only' => ['show', 'update', 'destroy'], 'parameters' => ['' => 'area']]);

        });

        //ConfigureAction
        Route::resource('configure-action', 'Customer\ConfigureActionController',['only' => ['edit', 'update', 'destroy']]);

        //DeviceGroup
        Route::resource('device-group', 'Customer\DeviceGroupController',['only' => ['edit', 'update', 'destroy']]);

        //Device
        Route::put('device/on-off/{device}',['uses' => 'Customer\DeviceController@onOffWorkerDevice']);
        Route::put('device/manual-on-off/{device}',['uses' => 'Customer\DeviceController@onOffManualControlWorkerDevice']);
        Route::resource('device', 'Customer\DeviceController',['only' => ['show', 'edit', 'update', 'destroy']]);

        //Admin GROUP
        Route::group(['prefix' => 'admin'], function (){
            //Users
            Route::resource('user', 'Admin\UserController', ['only' => ['index', 'show','store', 'update', 'destroy']]);

        });
    });


    Route::group(['prefix' => 'device/request', 'middleware' => 'device.check'], function (){
        Route::get('/worker', ['uses' => 'Device\DeviceRequestController@workerRequest']);
        Route::post('/publisher', ['uses' => 'Device\DeviceRequestController@publisherRequest']);
    });

});



