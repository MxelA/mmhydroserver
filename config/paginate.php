<?php

return [
    'dropdown' => [
        25 => 25,
        50 => 50,
        100 => 100,
        500 => 500,
    ],
    'default' => 25,
    'max' => 500
];
