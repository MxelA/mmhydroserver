<?php
namespace Deployer;
require 'recipe/laravel.php';
require 'vendor/deployer/recipes/local.php';
require 'vendor/deployer/recipes/rsync.php';
require 'vendor/deployer/recipes/npm.php';

// Configuration
set('ssh_type', 'native');
set('ssh_multiplexing', true);
set('writable_mode', 'chmod');
set('default_stage', 'dev');
set('local_deploy_path', '/tmp/deployer/h2m_solution');
set('repository', 'https://MxelA@bitbucket.org/MxelA/mmhydroserver.git');

add('shared_files', ['.env']);
add('shared_dirs', []);
add('writable_dirs', ['bootstrap', 'storage']);
add('rsync', [
    'exclude' => [
        '.git',
        '.salt',
        'Homestead.yaml',
        'Vagrantfile',
        'deploy.php',
        'node_modules'
    ],
    'timeout' => 1200
]);

// RSYNC files from /tmp/deployer
set('rsync_src', function() {
    $local_src = get('local_release_path');
    if(is_callable($local_src)){
        $local_src = $local_src();
    }
    return $local_src;
});

// Servers
server('production', 'h2m-solution.com')
    ->user('h2m-solution')
    ->identityFile()
    ->set('branch', 'master')
    ->set('deploy_path', '/home/h2m-solution/www')
    ->stage('production');



// Build assets locally
task('bower:install', function () {
    runLocally("cd {{local_release_path}} && bower install");
});

//Build Single Vue page app
task('npm:local:build', function () {
    runLocally("cd /home/alex/Sites/vuetifyjs/h2m-vuetify-app && {{local/bin/npm}} run build");
});

//Copy build to public dir
task('npm:local:copy:build', function () {
    runLocally("mkdir {{local_release_path}}/public/h2m-app");
    runLocally("cp -R /home/alex/Sites/vuetifyjs/h2m-vuetify-app/dist {{local_release_path}}/public/h2m-app");
});

// Run tests
task('local:phpunit', function () {
    runLocally("cd {{local_release_path}} && ~/.composer/vendor/bin/phpunit");
});

//Migrate module
task('artisan:module:migrate', function () {
    run('{{bin/php}} {{release_path}}/artisan module:migrate --force');
});

//Set ownership folder on server
task('deploy:set:ownership', function () {
    run('sudo chgrp -R www-data /home/h2m-solution/www/shared');
    run('sudo chmod -R ug+rwx /home/h2m-solution/www/shared');
});

//Reload php-fpm
task('reload:php-fpm', function () {
    run('sudo systemctl restart php7.1-fpm.service');
});



// Tasks
task('deploy', [
    'local:prepare',        // Create dirs locally
    'local:release',        // Release number locally
    'local:update_code',    // git clone locally
    'local:vendors',        // composer install locally
//    'local:phpunit',        // phpunit tests locally
//    'bower:install',        // bower install locally
//    'npm:local:install',    // npm install locally
    'npm:local:build',      // Build Vue Single page app locally
    'npm:local:copy:build', // Copy Build to public dir
    'local:symlink',        // Symlink /current locally
    'deploy:prepare',       // Create dirs on server
    'deploy:lock',          // Lock deploys on server
    'deploy:release',       // Release number on server
    'rsync',                // Send files to server
    'deploy:writable',      // Ensure paths are writable on server
    'deploy:shared',        // Shared and .env linking on server
    'deploy:set:ownership', // Set ownership folder on server
    'artisan:view:clear',   // Optimze on server
    'artisan:cache:clear',  // Optimze on server
    'artisan:config:cache', // Optimze on server
    'artisan:optimize',     // Optimze on server
    'artisan:migrate',      // Optimze on server
    'artisan:queue:restart', // Optimze on server
//    'artisan:module:migrate', // Migrate DB on server
    'deploy:symlink',         // Symlink /current on server
    'deploy:unlock',          // Unlock deploys on server
    'cleanup',                // Cleanup old releases on server
    'local:cleanup',           // Cleanup old releases locally
    'reload:php-fpm',          // Reload fpm


])->desc('Deploy your project');

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');