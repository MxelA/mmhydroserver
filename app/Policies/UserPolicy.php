<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $authUser
     * @return bool
     */
    public function index(User $authUser)
    {
        return $authUser->isAn(User::ROLE_ADMIN);
    }

    /**
     * @param User $authUser
     * @param User $user
     * @return bool
     */
    public function show(User $authUser, User $user)
    {
        if ( $authUser->isAn(User::ROLE_ADMIN) || $authUser->id === $user->id ) {
            return true;
        }

        return false;
    }

    /**
     * @param User $authUser
     * @return bool
     */
    public function store(User $authUser)
    {
        return $authUser->isAn(User::ROLE_ADMIN);
    }

    /**
     * @param User $authUser
     * @param User $user
     * @return bool
     */
    public function update(User $authUser, User $user)
    {
        if ( $authUser->isAn(User::ROLE_ADMIN) || $authUser->id === $user->id ) {
            return true;
        }

        return false;
    }

    /**
     * @param User $authUser
     * @return bool
     */
    public function destroy(User $authUser)
    {
        return $authUser->isAn(User::ROLE_ADMIN);
    }
}
