<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{


    public function scopeSortBy($query, $sortBy = 'id', $sortType = 'ASC')
    {
        return $query->orderBy($sortBy, $sortType);
    }

    public function scopeCriteriaRequest($query)
    {

        return $query->sortBy(request('sortBy', 'id'), sortType())
        ;

    }

}