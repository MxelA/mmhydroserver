<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Silber\Bouncer\Database\HasRolesAndAbilities;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{

    use Notifiable;
    use SoftDeletes;
    use HasRolesAndAbilities;

    const ROLE_ADMIN    = 'admin';
    const ROLE_DEVICE   = 'device';
    const ROLE_CUSTOMER = 'customer';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role', 'remember_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /************************
     *          SCOPES
     ************************/
    public function scopeSortBy($query, $sortBy = 'id', $sortType = 'ASC')
    {
        return $query->orderBy($sortBy, $sortType);
    }

    public function scopeCriteriaSearch($query, $search)
    {
        if($search) {
            $names = str_replace(' ', '|', trim($search));

            return  $query->where('name', 'REGEXP', $names)
                ->orWhere('email', 'LIKE', '%'.$search.'%')
                ;
        }

    }


    /************************
     *      RELATIONS
     ***********************/

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function areas()
    {
        return $this->hasMany(Area::class,'user_id');
    }
}
