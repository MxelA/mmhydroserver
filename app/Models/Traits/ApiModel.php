<?php
namespace App\Models\Traits;

use App\Models\SearchModel;

trait ApiModel {
    public static function vueTifyDataTablesPaginator(SearchModel $searchModel, Array $fields, $modelBuilder = null) {

        $items = new self();

        if($modelBuilder) {
            $items = $modelBuilder;
        }

        $items->select($fields);

        $perPage = $searchModel->perPage;

        if (isset($searchModel->sortBy) && in_array($searchModel->sortBy, $fields)) {
            $sortType = (isset($searchModel->descending) && $searchModel->descending === 'true') ? 'DESC' : 'ASC';
            $items     = $items->orderBy($searchModel->sortBy, $sortType);
        }

        $items = $items->paginate($perPage);

        return [
            'totalItems' => $items->total(),
            'items'      => $items->items(),
        ];
    }
}