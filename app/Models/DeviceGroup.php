<?php

namespace App\Models;

use App\Models\Traits\ApiModel;
use Illuminate\Database\Eloquent\Model;

class DeviceGroup extends BaseModel
{
    use ApiModel;

    protected $fillable = ['name', 'data_type', 'device_type'];

    /****************************
     * RELATIONS
     ***************************/
    public function area()
    {
        return $this->belongsTo(Area::class,'area_id');
    }

    public function devices()
    {
        return $this->belongsToMany(Device::class);
    }

    /***********************
     * SCOPES
     ***********************/
    public function scopeCriteriaByName($query, $name = null)
    {
        if($name) {
            return $query->where('name', 'like', '%' . $name . '%' );
        }
    }


    public static function saveDeviceGroups($items, Area $area, array $oldItemIDs = [])
    {
        if ( ! empty($items)) {
            $savedDeviceGroupIDs = [];

            foreach ($items as $sort => $item) {

                if ( (! isset($item['name']) || !$item['name'] ) || (! isset($item['data_type']) || !$item['data_type'] ) || (! isset($item['device_type']) || ! $item['device_type'] ) ) {
                    continue;
                }

                if (isset($item['id']) && $item['id']) {
                    $DeviceGroup = DeviceGroup::find($item['id']);
                } else {
                    $DeviceGroup = new DeviceGroup();
                }

                if ( ! is_array($item)) {
                    $item = $item->toArray();
                }


                $DeviceGroup->fill($item);
                $DeviceGroup->area()->associate($area);
                $DeviceGroup->save();

                array_push($savedDeviceGroupIDs, $DeviceGroup->id);
            }

            $deviceGroupIDsForDelete = array_diff($oldItemIDs, $savedDeviceGroupIDs);

            if ( ! empty($deviceGroupIDsForDelete))
            {
                foreach ($deviceGroupIDsForDelete as $devceGroupId)
                {
                    $deviceGroup = DeviceGroup::find($devceGroupId);
                    $deviceGroup->devices()->detach();
                    $deviceGroup->delete();
                }
            }
        }
    }
}
