<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SearchModel extends Model
{
    protected $guarded = [];

    public function __construct(array $attributes = [], array $defaults = [])
    {
        $attributes = array_merge($defaults, $attributes);

        parent::__construct($attributes);

        if (isset($attributes['rowPerPage']) && $attributes['rowPerPage'] && ($attributes['rowPerPage'] > 0 && $attributes['rowPerPage'] <= config('paginate.max'))) {
            $this->setPerPage((int) $attributes['rowPerPage']);
        } else {
            $this->setPerPage(config('paginate.default'));
        }
    }
}
