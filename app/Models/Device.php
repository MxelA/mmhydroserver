<?php

namespace App\Models;

use App\Models\Traits\ApiModel;

class Device extends BaseModel
{
    use ApiModel;

    const DEVICE_TYPE_PUBLISHER     = 'publisher';
    const DEVICE_TYPE_WORKER        = 'worker';

    const DEVICE_DATA_TYPE_INTEGER  = 'integer';
    const DEVICE_DATA_TYPE_FLOAT    = 'float';
    const DEVICE_DATA_TYPE_BOOLEAN  = 'boolean';

    public static $deviceTypes = [
        self::DEVICE_TYPE_PUBLISHER => 'Publisher',
        self::DEVICE_TYPE_WORKER    => 'Worker'
    ];

    public static $deviceDataTypes = [
        self::DEVICE_DATA_TYPE_INTEGER  => 'Integer',
        self::DEVICE_DATA_TYPE_FLOAT    => 'Float',
        self::DEVICE_DATA_TYPE_BOOLEAN  => 'Float',
    ];


    protected $appends  = ['current_data', 'average_data', 'last_data_send_date'];
    protected $fillable = ['name', 'data_type', 'device_type', 'device_group_id', 'unit', 'token', 'icon', 'manual_control', 'turn_on'];
    protected $casts    = [
        'manual_control' => 'boolean',
        'turn_on'        => 'boolean'
    ];


    /** RELATIONS **/
    public function area() {
        return $this->belongsTo(Area::class, 'area_id');
    }

    public function deviceGroup() {
        return $this->belongsTo(Area::class, 'device_group_id');
    }

    public function deviceGroups() {
        return $this->belongsToMany(DeviceGroup::class);
    }

    public function data() {
        return $this->hasMany(DeviceData::class, 'device_id');
    }
    /** END RELATIONS **/

    /** SCOPES **/
    public function scopeCriteriaByDeviceType($query, $deviceType)
    {
        return $query->where('device_type', '=', $deviceType);
    }

    public function scopeCriteriaByName($query, $name = null)
    {
        if($name) {
            return $query->where('name', 'like', '%' . $name . '%' );
        }
    }

    /** END SCOPES **/

    /** ACCESSORS */
    public function getCurrentDataAttribute()
    {
        $deviceData = $this->data()->latest()->first();
        if(! $deviceData) {
            return null;
        }

        $val = $deviceData->data;
        settype($val, $this->data_type);

        return $val;
    }

    public function setCurrentDataAttribute($val)
    {
        $this->attributes['current_data'] = $val;
        $this->current_data = $val;
    }

    public function getLastDataSendDateAttribute()
    {
        $deviceData = $this->data()->latest()->first();
        if(! $deviceData) {
            return null;
        }

        return $deviceData->created_at->toDateTimeString();
    }

    public function getAverageDataAttribute()
    {
        $val = $this->data()
            ->latest()
            ->limit(10)
            ->avg('data')
        ;

        settype($val, $this->data_type);

        return $val;
    }
    /** END ACCESSORS */

}
