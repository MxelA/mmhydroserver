<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Area extends BaseModel
{
    use SoftDeletes;

    protected $fillable = ['name', 'additional_data'];

    protected $casts = [
        'additional_data' => 'json'
    ];

    /**************************
     *      RELATIONS
     **************************/
    public function areaDevices()
    {
        return $this->hasMany(Device::class, 'area_id');
    }

    public function areaDeviceGroup()
    {
        return $this->hasMany(DeviceGroup::class, 'area_id');
    }

    public function areaConfigureActions()
    {
        return $this->hasMany(ConfigureAction::class, 'area_id');
    }

    /**
     * @param \App\Models\SearchModel|null $model
     * @return array|\Illuminate\Pagination\LengthAwarePaginator
     */
    public static function paginator(SearchModel $searchModel)
    {
        return self::vueTifyDataTablesPaginator($searchModel, ['id', 'name']);
    }
}
