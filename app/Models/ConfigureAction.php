<?php

namespace App\Models;

class ConfigureAction extends BaseModel
{
    protected $fillable = ['entity_id', 'entity_type', 'name', 'type', 'rules', 'action', 'applied'];

    protected $dates = ['applied'];
    protected $casts = [
        'action'    => 'json',
        'rules'     => 'json'
    ];

    /****************************
     * RELATIONS
     ***************************/
    public function entity()
    {
        return $this->morphTo('entity', 'entity_type', 'entity_id');
    }

    public function area()
    {
        return $this->belongsTo(Area::class);
    }


    /***********************
     * SCOPES
     ***********************/
    public function scopeCriteriaSearch($query, $search)
    {
        if($search) {
            return  $query->where('name', 'LIKE', '%'.$search.'%');
        }

    }
}
