<?php

if ( ! function_exists('paginationPerPage')) {
    function paginationPerPage()
    {
        $limit = config('paginate.default');

        switch (true) {
            case ( request('rowPerPage') && ( request('rowPerPage') > 0 && request('rowPerPage') <= config('paginate.max')) ):
                $limit = (int) request('rowPerPage');
                break;

            case ( request('limit') && ( request('limit') > 0 && request('limit') <= config('paginate.max')) ):
                $limit = (int) request('limit');
                break;

        }

        return (int) $limit;

    }
}

if ( ! function_exists('sortType')) {
    function sortType()
    {
        if(request('sortType', null)) {
            switch (request('sortType')) {
                case 'asc':
                    return 'ASC';
                    break;
                case 'desc':
                    return 'DESC';
                break;
                default:
                    return 'ASC';
            }
        }

        if(request('descending', null)) {
            switch (request('descending')) {
                case 'true':
                    return 'DESC';
                break;
                case 'false':
                    return 'ASC';
                break;
                default:
                    return 'ASC';
            }
        }

        return 'ASC';
    }
}