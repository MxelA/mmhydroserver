<?php

namespace App\Http\Requests\DeviceGroup;

use App\Models\Device;
use Illuminate\Foundation\Http\FormRequest;

class StoreDeviceGroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'        => 'required|min:3',
            'data_type'   => 'required|in:'. implode(',',[Device::DEVICE_DATA_TYPE_BOOLEAN, Device::DEVICE_DATA_TYPE_FLOAT, Device::DEVICE_DATA_TYPE_INTEGER]),
            'device_type' => 'required|in:'. implode(',',[Device::DEVICE_TYPE_WORKER, Device::DEVICE_TYPE_PUBLISHER]),
            'devices.*'   => 'sometimes|exists:devices,id',
        ];
    }
}
