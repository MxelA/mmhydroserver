<?php

namespace App\Http\Requests\Device;

use App\Models\Device;
use Illuminate\Foundation\Http\FormRequest;

class StoreDeviceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->sanitize();
        return [
            'name'            => 'required',
            'device_type'     => 'required|in:'. implode(',', [Device::DEVICE_TYPE_WORKER,Device::DEVICE_TYPE_PUBLISHER]),
            'data_type'       => 'required|in:'. implode(',', [Device::DEVICE_DATA_TYPE_BOOLEAN,Device::DEVICE_DATA_TYPE_FLOAT, Device::DEVICE_DATA_TYPE_INTEGER]),
        ];
    }

    private function sanitize(){
        $input = $this->all();

        $input['manual_control']    = null;
        $input['turn_on']           = null;
        $input['token']             = str_random(30);

        if($input['device_type'] && $input['device_type'] === Device::DEVICE_TYPE_WORKER) {
            $input['manual_control']    = false;
            $input['turn_on']           = false;
        }

        $this->replace($input);
    }

}
