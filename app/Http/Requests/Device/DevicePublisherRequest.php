<?php

namespace App\Http\Requests\Device;

use App\Http\Requests\ApiRequest;
use App\Models\Device;

class DevicePublisherRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'data'  => 'required',
            'token' => 'required'
        ];
    }

}
