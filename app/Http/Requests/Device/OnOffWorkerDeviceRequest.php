<?php

namespace App\Http\Requests\Device;

use Illuminate\Foundation\Http\FormRequest;

class OnOffWorkerDeviceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'turn_on' => 'required|boolean'
        ];
    }

}
