<?php

namespace App\Http\Requests\ConfigureAction;

use App\Services\ConfigureAction\ConfigureActionRuleDevicePublisher;
use App\Services\ConfigureAction\ConfigureActionRuleTime;
use App\Services\ConfigureAction\ConfigureActionService;
use Illuminate\Foundation\Http\FormRequest;

class StoreAreaDeviceConfigureActionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->sanitize();

        return [
            'entity_id'                 => 'required|integer',
            'entity_type'               => 'required|in:'.implode(',',[
                ConfigureActionService::$aliasEntityType[ConfigureActionService::TYPE_ENTITY_DEVICE],
//                ConfigureActionService::$aliasEntityType[ConfigureActionService::TYPE_ENTITY_DEVICE_GROUP]
            ]),
            'rules.*.type'              => 'required|in:'.implode(',', [
                ConfigureActionRuleTime::RULE,
                ConfigureActionRuleDevicePublisher::RULE
            ]),
            'action.turn_on'            => 'required|in:true,false',
//            'additional_data.date'      => 'required_if:type,'.ConfigureActionService::TYPE_TIME,
//            'additional_data.time'      => 'required_if:type,'.ConfigureActionService::TYPE_TIME,
//            'additional_data.repeat'    => 'required_if:type,'.ConfigureActionService::TYPE_TIME.'|in:'. implode(',', [
//                ConfigureActionService::TIME_REPEAT_DAY,
//                ConfigureActionService::TIME_REPEAT_WEEK,
//                ConfigureActionService::TIME_REPEAT_MONTH,
//            ]),

        ];
    }

    private function sanitize()
    {
        $inputData = $this->all();

        $inputData['applied'] = null;

        switch (true) {
            case (isset($inputData['entity_type']) && $inputData['entity_type'] && (!isset($inputData['name']) || !$inputData['name'])):
                $inputData['name'] = ucfirst($inputData['entity_type']) .'-' . $inputData['entity_id'];

            case (isset($inputData['entity_type']) && $inputData['entity_type']):
                $inputData['entity_type'] = array_search($inputData['entity_type'], ConfigureActionService::$aliasEntityType);
        }

        $this->replace($inputData);
    }

}
