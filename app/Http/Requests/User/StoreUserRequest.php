<?php

namespace App\Http\Requests\User;

use App\Http\Requests\ApiRequest;
use App\Models\User;

class StoreUserRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->sanitize();
        return [
            'name'      => 'required|min:3',
            'email'     => 'required|email|unique:users,email',
            'password'  => 'required|confirmed|min:6',
            'role'      => 'required|in:'. implode(',', [User::ROLE_ADMIN,User::ROLE_CUSTOMER])
        ];
    }

    private function sanitize(){
        $input = $this->all();

        if(isset($input['password']))
        {
            $input['password'] =  bcrypt($input['password']);
        }

        $input['remember_token'] = str_random(16);

        $this->replace($input);
    }

}
