<?php

namespace App\Http\Requests\User;

use App\Http\Requests\ApiRequest;
use App\Models\User;

class UpdateUserRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = $this->route('user');
        $this->sanitize();

        return [
            'name'      => 'required',
            'email'     => 'required|email|unique:users,email,' . $user->id,
            'password'  => 'confirmed|min:6',
            'role'      => 'required|in:'. implode(',', [User::ROLE_ADMIN,User::ROLE_CUSTOMER])
        ];
    }

    private function sanitize() {
        $input = $this->all();

        if(isset($input['password']))
        {
            $input['password'] =  bcrypt($input['password']);
        }

        if(! \Auth::user()->isAn(User::ROLE_ADMIN)) {
            unset($input['role']);
        }

        $this->replace($input);
    }

}
