<?php

namespace App\Http\Requests\Area;

use App\Http\Requests\ApiRequest;

class StoreAreaRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->sanitize();

        return [
            'name'                      => 'required|min:3',
            'additional_data.time_zone' => 'required'
        ];
    }

    private function sanitize()
    {
    }

}
