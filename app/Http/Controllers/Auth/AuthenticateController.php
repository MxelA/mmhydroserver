<?php

namespace App\Http\Controllers\Auth;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use JWTAuth;
use App\Http\Controllers\Controller;
use Namshi\JOSE\JWT;
use Tymon\JWTAuth\Exceptions\JWTException;

use Illuminate\Http\Request;

class AuthenticateController extends Controller
{
    public function authenticate(Request $request)
    {
        // grab credentials from the request
        $credentials = $request->only('email', 'password');

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'Username or password are wrong!'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'Could not create token!'], 500);
        }

        //Decode token
        \JWTAuth::setToken($token);
        $jwtToken = JWTAuth::getToken();
        $jwtDecodeToken = \JWTAuth::decode($jwtToken)->toArray();

        $user = \JWTAuth::authenticate();
        return response()->json(compact('token', 'user') );
    }
}
