<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Http\Requests\ConfigureAction\StoreAreaDeviceConfigureActionRequest;
use App\Http\Resources\ConfigureActionResource;
use App\Models\ConfigureAction;

class ConfigureActionController extends Controller
{
    /**
     * @param Device $device
     * @return ConfigureActionResource
     */
    public function edit(ConfigureAction $configureAction)
    {
        return new ConfigureActionResource($configureAction);
    }

    /**
     * @param UpdateDeviceRequest $request
     * @param Device $device
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(StoreAreaDeviceConfigureActionRequest $request, ConfigureAction $configureAction)
    {
        $configureAction->update($request->all());

        return response()->json(['success' => 'Configure action update successfully!']);
    }

    public function destroy(ConfigureAction $configureAction)
    {
        $configureAction->delete();
        return response()->json(['success' => 'Configure action delete successfully!']);
    }
}
