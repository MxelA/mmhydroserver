<?php

namespace App\Http\Controllers\Customer;

use App\Http\Requests\ConfigureAction\StoreAreaDeviceConfigureActionRequest;
use App\Http\Requests\DeviceGroup\StoreDeviceGroupRequest;
use App\Http\Resources\AreaResource;
use App\Http\Resources\ConfigureActionResource;
use App\Http\Resources\DeviceGroupResource;
use App\Http\Resources\DeviceResource;
use App\Models\Area;
use App\Models\Device;
use App\Models\SearchModel;
use Illuminate\Http\Request;
use App\Http\Requests\Area\StoreAreaRequest;
use App\Http\Requests\Device\StoreDeviceRequest;
use App\Http\Controllers\Controller;
use App\Models\User;


class AreaController extends Controller
{

    /**
     * @param Area $area
     * @return AreaResource
     */
    public function show(Area $area)
    {
        return new AreaResource($area);
    }

    /**
     * @param StoreAreaRequest $request
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreAreaRequest $request, User $user)
    {
        $user->areas()->create($request->all());

        return response()->json(['success' => 'Area store successfully!']);
    }

    /**
     * @param StoreAreaRequest $request
     * @param Area $area
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(StoreAreaRequest $request, Area $area)
    {
        $area->update($request->all());

        return response()->json(['success' => 'Area updated successfully!']);
    }

    /**
     * @param Area $area
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Area $area)
    {
        $area->delete();

        return response()->json(['success' => 'Area deleted successfully']);
    }


    /**
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function areasUser(Request $request, User $user)
    {
        $areas = $user
            ->areas()
            ->sortBy($request->input('sortBy', 'id'), sortType())
        ;

        return AreaResource::collection($areas->paginate(paginationPerPage()));
    }

    /****************************
     *      AREA DEVICES
     ***************************/

    /**
     * @param Request $request
     * @param Area $area
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function areaPublisherDevices(Request $request, Area $area)
    {
        $areaPublisherDevices = $area
            ->areaDevices()
            ->criteriaByName($request->input('search', null))
            ->criteriaByDeviceType(Device::DEVICE_TYPE_PUBLISHER)
            ->sortBy($request->input('sortBy', 'id'), sortType())
        ;

        return DeviceResource::collection($areaPublisherDevices->paginate(paginationPerPage()));
    }

    /**
     * @param Request $request
     * @param Area $area
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function areaWorkerDevices(Request $request, Area $area)
    {

        $areaWorkerDevices = $area
            ->areaDevices()
            ->criteriaByName($request->input('search', null))
            ->criteriaByDeviceType(Device::DEVICE_TYPE_WORKER)
            ->sortBy($request->input('sortBy', 'id'), sortType())
        ;

        return DeviceResource::collection($areaWorkerDevices->paginate(paginationPerPage()));
    }

    /**
     * @param StoreDeviceRequest $request
     * @param Area $area
     * @return DeviceResource
     */
    public function areaDeviceStore(StoreDeviceRequest $request, Area $area)
    {

        $device = Device::create($request->all());

        $device->area()->associate($area);
        $device->save();

        return new DeviceResource($device);
    }

    /********************************
     *      AREA CONFIGURE ACTION
     ********************************/

    /**
     * @param Request $request
     * @param Area $area
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function areaConfigureActionIndex(Request $request, Area $area)
    {
        $areaConfigureActions = $area
            ->areaConfigureActions()
            ->criteriaSearch($request->input('search'))
            ->sortBy($request->input('sortBy', 'id'), sortType())
        ;

        return ConfigureActionResource::collection($areaConfigureActions->paginate(paginationPerPage()));
    }

    /************************************
     * @param StoreAreaDeviceConfigureActionRequest $request
     * @param Area $area
     * @return ConfigureActionResource
     */
    public function areaConfigureActionStore(StoreAreaDeviceConfigureActionRequest $request, Area $area)
    {
        $configureAction = $area->areaConfigureActions()->create($request->all());

        return new ConfigureActionResource($configureAction);
    }

    /*************************************
     *          AREA DEVICE GROUP
     ************************************/

    /**
     * @param Request $request
     * @param Area $area
     * @return \Illuminate\Http\JsonResponse
     */
    public function areaDeviceGroupIndex(Request $request, Area $area)
    {
        $areaGroups = $area
            ->areaDeviceGroup()
            ->criteriaByName($request->input('search', null))
            ->sortBy($request->input('sortBy', 'id'), sortType())
        ;

        return DeviceGroupResource::collection($areaGroups->paginate(paginationPerPage()));
    }

    /**
     * @param Request $request
     * @param Area $area
     * @return \Illuminate\Http\JsonResponse
     */
    public function areaDeviceGroupStore(StoreDeviceGroupRequest $request, Area $area)
    {
        $area->areaDeviceGroup()->create($request->all());

        return response()->json(['success' => 'Device Groups successfully saved!']);
    }
}
