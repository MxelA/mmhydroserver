<?php

namespace App\Http\Controllers\Customer;

use App\Http\Requests\Device\OnOffWorkerDeviceRequest;
use App\Http\Resources\DeviceResource;
use App\Models\Device;
use App\Http\Controllers\Controller;
use App\Http\Requests\Device\UpdateDeviceRequest;
use App\Http\Requests\Device\OnOffManualControlWorkerDeviceRequest;

class DeviceController extends Controller
{
    public function show(Device $device)
    {
        return response()->json($device->load(['deviceGroups' => function($query){
            $query->select('id');
        }]));
    }

    /**
     * @param Device $device
     * @return DeviceResource
     */
    public function edit(Device $device)
    {
        return new DeviceResource($device);
    }


    /**
     * @param UpdateDeviceRequest $request
     * @param Device $device
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateDeviceRequest $request, Device $device)
    {

        $deviceGroupsIds = collect($request->input('device_groups'), [])->pluck('id')->toArray();
        $device->update($request->all());
        $device->deviceGroups()->sync($deviceGroupsIds);


        return response()->json(['success' => 'Device update successfully!']);
    }

    /**
     * @param Device $device
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Device $device)
    {
        $device->delete();
        return response()->json(['success' => 'Device delete successfully!']);
    }

    /**
     * @param ManualSetWorkerRequest $request
     * @param Device $device
     * @return \Illuminate\Http\JsonResponse
     */
    public function onOffManualControlWorkerDevice(OnOffManualControlWorkerDeviceRequest $request, Device $device)
    {
        if($device->device_type !== Device::DEVICE_TYPE_WORKER) {
            return response()->json(['error' => 'This is not worker'], 422);
        }

        $device->update(['manual_control' => $request->input('manual_control')]);

        return response()->json(['success' => 'Manual set Worker is successfully!']);
    }

    /**
     * @param TurnOnWorkerDeviceRequest $request
     * @param Device $device
     * @return \Illuminate\Http\JsonResponse
     */
    public function onOffWorkerDevice(OnOffWorkerDeviceRequest $request, Device $device)
    {
        if($device->device_type !== Device::DEVICE_TYPE_WORKER) {
            return response()->json(['error' => 'This is not worker'], 422);
        }

        $device->update(['turn_on' => $request->input('turn_on')]);

        return response()->json(['success' => 'Device Worker is on/off successfully!']);
    }
}
