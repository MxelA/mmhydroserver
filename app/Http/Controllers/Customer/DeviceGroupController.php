<?php

namespace App\Http\Controllers\Customer;

use App\Http\Requests\DeviceGroup\StoreDeviceGroupRequest;
use App\Http\Resources\DeviceGroupResource;
use App\Models\DeviceGroup;
use App\Http\Controllers\Controller;

class DeviceGroupController extends Controller
{

    /**
     * @param DeviceGroup $deviceGroup
     * @return DeviceGroupResource
     */
    public function edit(DeviceGroup $deviceGroup)
    {
        return new DeviceGroupResource($deviceGroup);
    }

    /**
     * @param StoreDeviceGroupRequest $request
     * @param DeviceGroup $deviceGroup
     * @return DeviceGroupResource
     */
    public function update(StoreDeviceGroupRequest $request, DeviceGroup $deviceGroup)
    {
        $deviceGroup->update($request->except('area_id'));
        $deviceGroup->devices()->sync($request->input('devices', []));

        return new DeviceGroupResource($deviceGroup->fresh());
    }

    /**
     * @param DeviceGroup $deviceGroup
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(DeviceGroup $deviceGroup)
    {
        $deviceGroup->devices()->detach();
        $deviceGroup->delete();

        return response()->json(['success' => 'Device Groups successfully deleted!']);
    }
}
