<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\User\StoreUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Http\Resources\UserResource;
use App\Models\SearchModel;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $this->authorize('index', User::class);


        $users = User::criteriaSearch($request->input('search', null))
            ->sortBy($request->input('sortBy', 'id'), sortType())
        ;

        return UserResource::collection($users->paginate(paginationPerPage()));
    }


    /**
     * @param User $user
     * @return UserResource
     */
    public function show(User $user)
    {
        $this->authorize('show', $user);

        return new UserResource($user);
    }

    /**
     * @param StoreUserRequest $request
     * @return UserResource
     */
    public function store(StoreUserRequest $request)
    {

        $this->authorize('store', User::class);

        $user = User::create($request->all());

        return new UserResource($user);
    }

    /**
     * @param UpdateUserRequest $request
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        $this->authorize('update', $user);

        $user->retract($user->role);
        $user->update($request->all());
        $user->assign($request->input('role', $user->role));

        return response()->json(['success' => 'User successfully updated!']);
    }

    /**
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(User $user)
    {
        $this->authorize('destroy', User::class);

        $user->retract($user->role);
        $user->delete();

        return response()->json(['success' => 'User successfully deleted!']);
    }
}
