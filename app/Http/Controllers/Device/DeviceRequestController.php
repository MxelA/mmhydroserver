<?php

namespace App\Http\Controllers\Device;

use App\Http\Controllers\Controller;
use App\Http\Requests\Device\DevicePublisherRequest;
use App\Jobs\DevicePublisherProcessingDataJob;
use App\Jobs\DeviceWorkerCheckAutomaticActionJob;
use App\Models\Device;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class DeviceRequestController extends Controller
{
    /**
     * @param DevicePublisherRequest $request
     * @param Device $device
     * @return \Illuminate\Http\JsonResponse
     */
    public function publisherRequest(DevicePublisherRequest $request, Device $device)
    {
        $data = $request->input('data');
        $job  = (new DevicePublisherProcessingDataJob($device, $data))->onQueue('device_publisher_processing_data');

        dispatch($job);

        return response()->json(['success' => 'Device data successfully received']);
    }

    /**
     * @param Request $request
     */
    public function workerRequest(Device $device)
    {
        if($device->device_type !== Device::DEVICE_TYPE_WORKER) {
            return new JsonResponse('Device is not worker', 422);
        }

        $job  = (new DeviceWorkerCheckAutomaticActionJob($device))->onQueue('device_worker_configure_actions');
        dispatch($job);

        return response()->json($device->turn_on, 200);
    }


}
