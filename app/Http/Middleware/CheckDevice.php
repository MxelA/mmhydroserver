<?php

namespace App\Http\Middleware;

use App\Models\Device;
use Closure;

class CheckDevice
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $device = Device::where('token', '=', $request->input('token'))->firstOrFail();
        $request->route()->setParameter('App\Models\Device', $device);
        return $next($request);
    }
}
