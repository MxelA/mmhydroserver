<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class DeviceResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => (int)        $this->id,
            'name'              => (string)     $this->name,
            'data_type'         => (string)     $this->data_type,
            'device_type'       => (string)     $this->device_type,
            'token'             => (string)     $this->token,
            'unit'              => (string)     $this->unit,
            'icon'              => (string)     $this->icon,
            'manual_control'    => (boolean)    $this->manual_control,
            'turn_on'           => (boolean)    $this->turn_on,
            'current_data'      =>              $this->current_data,
            'average_data'      =>              $this->average_data
        ];
    }
}
