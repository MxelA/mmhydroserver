<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class AreaResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => (int)        $this->id,
            'user_id'           => (int)        $this->user_id,
            'name'              => (string)     $this->name,
            'additional_data'   => (object)     $this->additional_data
        ];
    }
}
