<?php

namespace App\Http\Resources;

use App\Services\ConfigureAction\ConfigureActionService;
use Illuminate\Http\Resources\Json\Resource;

class ConfigureActionResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => (int)        $this->id,
            'area_id'           => (int)        $this->area_id,
            'entity_id'         => (int)        $this->entity_id,
            'entity_type'       => (string)     ConfigureActionService::$aliasEntityType[$this->entity_type],
            'name'              => (string)     $this->name,
            'action'            => (array)      $this->action,
            'rules'             => (array)      $this->rules,
            'applied'           => (string)     $this->applied,
        ];
    }
}
