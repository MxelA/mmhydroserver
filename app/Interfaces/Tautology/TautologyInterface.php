<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 9/3/18
 * Time: 10:28 PM
 */

namespace App\Interfaces\Tautology;


interface TautologyInterface
{
    public function checkStatementsValue(): bool;
}