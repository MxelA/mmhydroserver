<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 9/3/18
 * Time: 10:06 PM
 */

namespace App\Interfaces\ConfigureAction;

interface ConfigureActionRuleInterface {
    public function checkRules(): bool ;
}