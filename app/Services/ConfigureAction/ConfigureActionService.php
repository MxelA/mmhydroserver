<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 8/25/18
 * Time: 5:10 PM
 */

namespace App\Services\ConfigureAction;


use App\Models\ConfigureAction;
use App\Models\Device;
use App\Models\DeviceGroup;
use App\Services\Tautology\Tautology;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ConfigureActionService
{

    const TYPE_ENTITY_DEVICE        = Device::class;
    const TYPE_ENTITY_DEVICE_GROUP  = DeviceGroup::class;


    public static $aliasEntityType = [
        self::TYPE_ENTITY_DEVICE        => 'device',
        self::TYPE_ENTITY_DEVICE_GROUP  =>'device-group'
    ];


    /**
     * Get Time zone From entity type
     * @param ConfigureAction $configureAction
     * @return mixed
     */
    private function getTimeZone(ConfigureAction $configureAction)
    {
        switch (true) {
            case Device::class === $configureAction->entity_type:
                return $configureAction->entity->area->additional_data['time_zone'];
            break;
        }
    }

    /**
     * Get statement form rules in configure action
     * @param ConfigureAction $configureAction
     * @return bool
     */
    public function getConfigureActionStatementForRules(ConfigureAction $configureAction): bool
    {

        $configureActionRulesStatement  = [];
        foreach ($configureAction->rules as $rule) {
            switch (true) {
                case (ConfigureActionRuleTime::RULE === $rule['type']):
                    $timeZone                           = $configureAction->area->additional_data['time_zone'];
                    $configureActionRulesStatement[]    = (new ConfigureActionRuleTime($rule['date'], $rule['time'], $rule['repeat'], $timeZone, $configureAction->applied))
                        ->checkRules()
                    ;

                break;
                case (ConfigureActionRuleDevicePublisher::RULE === $rule['type']):
                    $configureActionRulesStatement[]    = (new ConfigureActionRhuleDevicePublisher($rule['device_id'], $rule['conditional_operator'], $rule['value']))->checkRules();

                break;
            }

        }

        return (new Tautology($configureActionRulesStatement, '&&'))->checkStatementsValue();
    }

    /**
     * Check configure action for specific entity type
     * @param Model $model
     */
    public function checkEntityConfigureActions(Model $model)
    {
        switch (get_class($model)) {
            case $this::TYPE_ENTITY_DEVICE:

                $configureActionDeviceEntity = new ConfigureActionEntityDevice($model);
                $configureActionDeviceEntity->process();

            break;
        }
    }

    /**
     * Set applied date time
     * @param ConfigureAction $configureAction
     */
    public function configureActionApplied(ConfigureAction $configureAction)
    {
       $configureAction->update(['applied' => Carbon::now('UTC')]);
    }
}