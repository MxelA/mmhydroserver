<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 8/25/18
 * Time: 5:10 PM
 */

namespace App\Services\ConfigureAction;

use App\Interfaces\ConfigureAction\ConfigureActionRuleInterface;
use Illuminate\Support\Carbon;

class ConfigureActionRuleTime implements ConfigureActionRuleInterface
{

    const RULE                      = 'time';

    const TIME_NO_REPEAT            = 'no-repeat';
    const TIME_REPEAT_DAY           = 'every-day';
    const TIME_REPEAT_WEEK          = 'every-week';
    const TIME_REPEAT_MONTH         = 'every-month';


    protected $date;
    protected $time;
    protected $repeat;
    protected $dateTime;
    protected $timeZone;
    protected $lastTimeWhenRuleApplied;


    /**
     * ConfigureActionRuleTime constructor.
     * @param $date
     * @param $time
     * @param $repeat
     */
    public function __construct($date, $time, $repeat, $timeZone, Carbon $lastTimeWhenRuleApplied = null)
    {
        $this->date                     = $date;
        $this->time                     = $time;
        $this->repeat                   = $repeat;
        $this->timeZone                 = $timeZone;
        $this->dateTime                 = Carbon::createFromFormat('Y-m-d g:ia', $this->date . ' ' . $this->time, $timeZone)->tz('UTC');
        $this->lastTimeWhenRuleApplied  = $lastTimeWhenRuleApplied;


        if( !in_array($this->repeat, [$this::TIME_REPEAT_DAY, $this::TIME_REPEAT_WEEK, $this::TIME_REPEAT_MONTH, $this::TIME_NO_REPEAT]) ) {
            throw new \Exception('Repeat property is not valid');
        }
    }

    public function checkRules(): bool
    {
        $currentDateTime = Carbon::now('UTC');

        switch ($this->repeat) {

            case $this::TIME_REPEAT_DAY:
                if($this->lastTimeWhenRuleApplied) {
                    $this->lastTimeWhenRuleApplied->addDay()->tz($this->timeZone)->setTimeFromTimeString($this->time);
                }

                break;

            case $this::TIME_REPEAT_WEEK:
                if($this->lastTimeWhenRuleApplied) {
                    $this->lastTimeWhenRuleApplied->addWeek()->tz($this->timeZone)->setTimeFromTimeString($this->time);
                }
                break;

            case $this::TIME_REPEAT_MONTH:
                if($this->lastTimeWhenRuleApplied) {
                    $this->lastTimeWhenRuleApplied->addMonth()->tz($this->timeZone)->setTimeFromTimeString($this->time);
                }
                break;

            case $this::TIME_NO_REPEAT:

                if( !$this->lastTimeWhenRuleApplied) {
                    if($currentDateTime->timestamp >= $this->dateTime->timestamp ) {
                        return true;
                    }
                }


                return false;
            break;
        }

        if ($currentDateTime->timestamp >= ($this->lastTimeWhenRuleApplied? $this->lastTimeWhenRuleApplied->timestamp: $this->dateTime->timestamp)) {
            return true;
        }

        return false;
    }
}