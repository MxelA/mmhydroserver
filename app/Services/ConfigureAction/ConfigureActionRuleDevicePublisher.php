<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 8/25/18
 * Time: 5:10 PM
 */

namespace App\Services\ConfigureAction;

use App\Interfaces\ConfigureAction\ConfigureActionRuleInterface;
use Illuminate\Support\Carbon;

class ConfigureActionRuleDevicePublisher implements ConfigureActionRuleInterface
{

    const RULE = 'device-publisher';

    protected $deviceId;
    protected $conditionalOperator;
    protected $value;


    /**
     * ConfigureActionRuleTime constructor.
     * @param $date
     * @param $time
     * @param $repeat
     */
    public function __construct($deviceId, $conditionalOperator, $value)
    {
        $this->deviceId                 = $deviceId;
        $this->conditionalOperator      = $conditionalOperator;
        $this->value                    = $value;

    }

    public function checkRules(): bool
    {
        return false;
    }
}