<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 8/25/18
 * Time: 5:10 PM
 */

namespace App\Services\ConfigureAction;

use App\Models\ConfigureAction;
use App\Models\Device;
use App\Services\Tautology\Tautology;

class ConfigureActionEntityDevice
{

    protected $device;
    protected $configureActions = [];
    protected $configureActionsStatement = [];

    /**
     * ConfigureActionRuleTime constructor.
     * @param $date
     * @param $time
     * @param $repeat
     */
    public function __construct(Device $device)
    {
        $this->device = $device;
    }


    /**
     * Processing configure actions for device
     *
     * @return bool
     */
    public function process(): bool
    {
        $this->configureActions = $deviceConfigureActions = ConfigureAction::where('entity_id', $this->device->id)
            ->where('entity_type', get_class($this->device))
            ->get()
        ;

        foreach ($deviceConfigureActions->groupBy('action') as $groupDeviceConfigureActions) {
            $this->configureActionsStatement = [];

            foreach ($groupDeviceConfigureActions as $configureAction) {
                $this->configureActionsStatement[$configureAction->id] = (new ConfigureActionService())->getConfigureActionStatementForRules($configureAction);
            }

            $statement = (new Tautology($this->configureActionsStatement, '||'))->checkStatementsValue();

            if ($statement === true) {
                return $this->applyConfigureActions();
            }

        }

        return false;
    }

    /**
     * Apply Configure Action
     * @return bool
     */
    private function applyConfigureActions()
    {
        foreach ($this->configureActionsStatement as $configureActionId => $configureActionState) {

            if($configureActionState === true) {

                $configureAction = $this->configureActions->where('id', $configureActionId)->first();
                (new ConfigureActionService())->configureActionApplied($configureAction);

                switch (true) {
                    case ($configureAction->action['turn_on'] === "true"):
                        return $this->device->update(['turn_on' => true]);
                        break;

                    case ($configureAction->action['turn_on'] === "false"):
                        return $this->device->update(['turn_on' => false]);
                        break;

                }
            }

        }
    }
}