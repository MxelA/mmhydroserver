<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 9/3/18
 * Time: 10:19 PM
 */

namespace App\Services\Tautology;


use App\Interfaces\Tautology\TautologyInterface;

class TautologyAnd implements TautologyInterface
{
    protected $statements;

    public function __construct(array $statements)
    {
        $this->statements = $statements;
    }

    public function checkStatementsValue(): bool
    {
        $returnStatement = null;

        foreach ($this->statements as $statement) {

            if($returnStatement === null) {
                $returnStatement = $statement;
            }

            $returnStatement = ($returnStatement && $statement);
        }

        return $returnStatement;
    }
}