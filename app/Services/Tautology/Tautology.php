<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 9/3/18
 * Time: 10:19 PM
 */

namespace App\Services\Tautology;

use App\Services\Tautology\TautologyOr;


class Tautology
{
    protected $statements;
    protected $operator;

    public function __construct(array $statements, $operator)
    {
        $this->statements   = $statements;
        $this->operator     = $operator;
    }

    public function checkStatementsValue()
    {
        switch ($this->operator) {
            case '||':
                 return (new TautologyOr($this->statements))->checkStatementsValue();
            break;

            case '&&':
                 return (new TautologyAnd($this->statements))->checkStatementsValue();
            break;

            default:
                throw new \Exception('Not support operator!');
        }
    }
}