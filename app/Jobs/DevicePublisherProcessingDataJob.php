<?php

namespace App\Jobs;

use App\Models\Device;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Events\DevicePublisherDataEvent;

class DevicePublisherProcessingDataJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $device;
    public $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Device $device, $data)
    {
        $this->device = $device;
        $this->data   = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $lastData           = $this->device->data()->orderBy('id', "DESC")->first();
        $dateTimeOfLastData = $lastData ? $lastData->created_at : Carbon::now();

        if( ! $lastData || $dateTimeOfLastData->diffInHours(Carbon::now()) >= 1) {
            $this->device->data()->create(['data' => $this->data]);
        }

        $serializeDevice = $this->device->toArray();
        $serializeDevice['current_data']            = $this->data;
        $serializeDevice['last_data_send_date']     = Carbon::now()->toDateTimeString();

        event(new DevicePublisherDataEvent($serializeDevice));

        return 1;
    }
}
