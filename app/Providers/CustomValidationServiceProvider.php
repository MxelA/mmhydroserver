<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class CustomValidationServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {

        Validator::extend('array_check', function ($attribute, $value, $parameters, $validator) {
            foreach ($value as $item) {
                if (0 !== count(array_diff($parameters, array_keys($item)))) {
                    $validator->errors()->add("$attribute", $attribute.' is not valid.  must contain '.implode(',', $parameters));
                } elseif (in_array(null, array_values($parameters))) {
                    $validator->errors()->add("$attribute", $attribute.'  is not valid. '.implode(',', $parameters).' can\'t be empty!');
                }
            }

            return true;
        });
    }

    /**
     * Register the application services.
     */
    public function register()
    {
    }
}
